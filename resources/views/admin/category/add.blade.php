@extends('admin.layout.master')

@section('content')

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="#">Category</a>
                    </li>
                    <li class="active">Add form</li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="page-header">
                    <h1>
                        Category
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Add
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                        <!-- PAGE CONTENT BEGINS -->
                        <form action="{{route($base_route.'.store')}}" class="form-horizontal" role="form" id="validation-form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>

                                <div class="col-sm-9">
                                    <input type="text" name="title" id="" placeholder="Title" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Slug </label>--}}

                                {{--<div class="col-sm-9">--}}
                                    {{--<input type="text" name="slug" id="" placeholder="Title" class="col-xs-10 col-sm-5">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image</label>

                                <div class="col-sm-9">
                                    <input type="file" name="image" id="" placeholder="Image" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Status</label>

                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input name="form-field-radio" type="radio" class="ace" />
                                            <span class="lbl"> Active</span>
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input name="form-field-radio" type="radio" class="ace" />
                                            <span class="lbl"> Inactive</span>
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Submit
                                    </button>

                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </form>

                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->


    @endsection

@section('js')

    <script src="{{asset('admin-panel/assets/js/jquery.validate.min.js')}}"></script>
  <script>
      $(document).ready(function () {
          $('#validation-form').validate();
      })
  </script>
    @endsection

