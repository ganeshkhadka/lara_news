<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Dashboard - Ace Admin</title>

    <meta name="description" content="overview &amp; stats" />
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('admin-panel/assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/fonts.googleapis.com.css')}}" />

    <!-- text fonts -->
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/ace-skins.min.css')}}" />
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/ace-rtl.min.css')}}" />

    <!--[if lte IE 9]>
    <![endif]-->
    <link rel="stylesheet" href="{{asset('admin-panel/assets/css/ace-ie.min.css')}}" />

    <!--[if lte IE 9]>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="{{asset('admin-panel/assets/js/ace-extra.min.js')}}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="{{asset('admin-panel/assets/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('admin-panel/assets/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

@yield('css')