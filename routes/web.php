<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin/', 'as'=>'admin.','middleware' => ['auth','auth-custom']],function (){

    Route::get('dashboard',              ['as'=>'dashboard','uses' => 'Admin\DashboardController@index']);

    Route::get('user',                   ['as'=>'user','uses' => 'Admin\UserController@index']);
    Route::get('user/add',               ['as'=>'user.add','uses' => 'Admin\UserController@add']);
    Route::post('user/store',            ['as'=>'user.store','uses' => 'Admin\UserController@store']);
    Route::get('user/edit/{id}',         ['as'=>'user.edit','uses' => 'Admin\UserController@edit']);
    Route::post('user/update/{id}',      ['as'=>'user.update','uses' => 'Admin\UserController@update']);
    Route::get('user/delete/{id}',       ['as'=>'user.delete','uses' => 'Admin\UserController@delete']);
    Route::get('user/password-reset',    ['as'=>'user.password-reset','uses' => 'Admin\UserController@passwordReset']);
    Route::post('user/password-reset',   ['as'=>'user.password-reset.update','uses' => 'Admin\UserController@passwordUpdate']);

    Route::get('site-profile/edit',      ['as'=>'site-profile.edit','uses' => 'Admin\SiteProfileController@edit']);
    Route::post('site-profile/update',   ['as'=>'site-profile.update','uses' => 'Admin\SiteProfileController@update']);

    Route::get('category',                   ['as'=>'category','uses' => 'Admin\CategoryController@index']);
    Route::get('category/add',               ['as'=>'category.add','uses' => 'Admin\CategoryController@add']);
    Route::post('category/store',            ['as'=>'category.store','uses' => 'Admin\CategoryController@store']);
    Route::get('category/edit/{id}',         ['as'=>'category.edit','uses' => 'Admin\CategoryController@edit']);
    Route::post('category/update/{id}',      ['as'=>'category.update','uses' => 'Admin\CategoryController@update']);
    Route::get('category/delete/{id}',       ['as'=>'category.delete','uses' => 'Admin\CategoryController@delete']);

    Route::get('news',                   ['as'=>'news','uses' => 'Admin\NewsController@index']);
    Route::get('news/add',               ['as'=>'news.add','uses' => 'Admin\NewsController@add']);
    Route::post('news/store',            ['as'=>'news.store','uses' => 'Admin\NewsController@store']);
    Route::get('news/edit/{id}',         ['as'=>'news.edit','uses' => 'Admin\NewsController@edit']);
    Route::post('news/update/{id}',      ['as'=>'news.update','uses' => 'Admin\NewsController@update']);
    Route::get('news/delete/{id}',       ['as'=>'news.delete','uses' => 'Admin\NewsController@delete']);
});
// part 8 1.45.12 mins completed
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
