<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['title','slug','image','writer','short_desc','detail_desc','publish_date','status'];
}
