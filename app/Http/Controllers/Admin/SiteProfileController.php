<?php

namespace App\Http\Controllers\Admin;
use App\SiteProfile;
use Illuminate\Support\Str;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteProfileController extends AdminBaseController
{
   protected $view_path = 'admin.site-profile';
   protected $base_route = 'admin.site-profile';
   protected $panel = 'User Profile';



    public function edit(Request $request)
    {
        $siteprofile = SiteProfile::first();
//        dd($siteprofile);
        return view($this->view_path.'.edit',compact('siteprofile'));
    }


    public function update(Request $request)
    {
//        dd($request->all());
//        $request -> validate([
//            'domain' => 'required',
////          'status' => 'required',
//        ]);
        $siteprofile = SiteProfile::first();
        $siteprofile -> domain = $request->get('domain');
//        dd($siteprofile);
        $siteprofile -> company_name = $request->get('company_name');
        if($request->hasfile('logo')){
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file -> move('uploads/gallery/', $filename);
            $siteprofile->logo = $filename;
        }
        $siteprofile -> save();
        return redirect()->route($this->base_route);
    }
}