<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PasswordResetFormValidation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//       $users=User::select('id','email','created_at','status');
        $users = DB::table('users')->select('id','username','email','contact_no','created_at','status')->get();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(AddFormValidation $request)
//    {
//        $request->request->add(['status' => $request->get('status')=='active'?1:0]);
//    User::create($request->all());
//    $request->session()->flash('success_message','User added Successfully');
//    return redirect()->route('admin.user');
//    }

    public function store(Request $request)
    {
       $request -> validate([
          'username' => 'required',
          'email' => 'required',
          'password' => 'required',
//          'full_name' => 'required',
//          'contact_no' => 'required',
          'address' => 'required',
//          'status' => 'required',
       ]);
       $user = new User();
       $user -> username = $request->get('username');
       $user -> email = $request->get('email');
       $user -> password = $request->get('password');
       $user -> full_name = $request->get('fullname');
       $user -> contact_no = $request->get('contact');
       $user -> address = $request->get('address');
//       $user -> status = $request->get('status') == 'active'?1:0;
       $user -> save();
       return redirect()->route('admin.user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request -> validate([
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
//          'full_name' => 'required',
//          'contact_no' => 'required',
            'address' => 'required',
//          'status' => 'required',
        ]);
        $user = User::find($id);
        $user -> username = $request->get('username');
        $user -> email = $request->get('email');
        $user -> password = $request->get('password');
        $user -> full_name = $request->get('fullname');
        $user -> contact_no = $request->get('contact');
        $user -> address = $request->get('address');
        $user -> save();
        return redirect()->route('admin.user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::find($id);
        $user -> delete();
        return redirect()->route('admin.user');
    }

    public function passwordReset()
    {
        return view('admin.user.password_reset');
    }

    public function passwordUpdate(PasswordResetFormValidation $request)
    {
//        dd($request->all());
    $user = auth()->user();
    $user -> password = bcrypt($request->get('password'));
    $user -> save();
//    $request->session()->flash('success_message','Password reset successfully');
    return redirect()->route('admin.user')->with('message', 'IT WORKS!');
    }
}