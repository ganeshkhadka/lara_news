<?php

namespace App\Http\Controllers\Admin;
use App\News;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends AdminBaseController
{
    protected $view_path = 'admin.news';
    protected $base_route = 'admin.news';
    protected $panel = 'News';
    public function index()
    {
//       $categories=Category::select('title','slug','image','status');
//       dd($categories);
        $news = DB::table('news')->select('id','title','slug','image','writer','short_desc','detail_desc','publish_date','status')->get();
        return view(parent::loadDataToView($this->view_path.'.index'),compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view(parent::loadDataToView($this->view_path.'.add'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $request -> validate([
            'title' => 'required',
//            'image' => 'required',
//            'status' => 'required',

        ]);
        $news = new News();
        $news -> title = $request->get('title');
        $news -> slug = Str::slug($request->get('title'));
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file -> move('uploads/news/', $filename);
            $news->image = $filename;
        }else{
//            return $request;
            $news->image = '';
        }

        $news -> writer = $request->get('writer');
        $news -> short_desc = $request->get('short_desc');
        $news -> detail_desc = $request->get('detail_desc');
        $news -> publish_date = $request->get('date');
//        return $request;
        $news -> status = $request->get('status') == 'active'?1:0;
        $news -> save();
        return redirect()->route($this->base_route);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view($this->view_path.'.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request -> validate([
            'title' => 'required',
//          'status' => 'required',
        ]);
        $news = News::find($id);
        $news -> title = $request->get('title');
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file -> move('uploads/news/', $filename);
            $news->image = $filename;
        }
        $news -> save();
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $news= News::find($id);
        $news -> delete();
        return redirect()->route($this->base_route);
    }
}