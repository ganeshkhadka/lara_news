<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Str;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends AdminBaseController
{
   protected $view_path = 'admin.category';
   protected $base_route = 'admin.category';
   protected $panel = 'Category';
    public function index()
    {
//       $categories=Category::select('title','slug','image','status');
//       dd($categories);
        $categories = DB::table('categories')->select('id','title','slug','image','status')->get();
        return view(parent::loadDataToView($this->view_path.'.index'),compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view(parent::loadDataToView($this->view_path.'.add'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'title' => 'required',
//            'image' => 'required',
//            'status' => 'required',

        ]);
        $category = new Category();
        $category -> title = $request->get('title');
        $category -> slug = Str::slug($request->get('title'));
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file -> move('uploads/gallery/', $filename);
            $category->image = $filename;
        }else{
//            return $request;
            $category->image = '';
        }
//        return $request;
       $category -> status = $request->get('status') == 'active'?1:0;
        $category -> save();
        return redirect()->route($this->base_route);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view($this->view_path.'.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request -> validate([
            'title' => 'required',
//          'status' => 'required',
        ]);
        $category = Category::find($id);
        $category -> title = $request->get('title');
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file -> move('uploads/gallery/', $filename);
            $category->image = $filename;
        }
        $category -> save();
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $category= Category::find($id);
        $category -> delete();
         return redirect()->route($this->base_route);
    }
}