<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteProfile extends Model
{
    protected $table = 'site_profiles';
    protected $fillable = ['domain','company_name','logo'];
}
