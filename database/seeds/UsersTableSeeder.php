<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
           'created_at' => \Carbon\Carbon::now(),
          'username' => 'ganeshkhadka',
           'email' => 'ganeshkhadka46@gmail.com',
           'password' => bcrypt('admin123'),
           'full_name' => 'Ganesh Khadka',
           'contact_no' => '98666567794',
           'address' => 'Butwal',
           'status' => 1,
       ]);
    }
}
